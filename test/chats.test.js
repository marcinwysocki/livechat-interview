const { ChatsService } = require("../services/chats");
const { UserNotFoundError } = require("../services/users");
const { USERS, MESSAGES } = require("./mocks");

describe("ChatsService", () => {
  let usersServiceMock;
  let messagesServiceMock;

  beforeEach(() => {
    usersServiceMock = {
      getById: jest.fn(uuid =>
        !!USERS[uuid]
          ? Promise.resolve(USERS[uuid])
          : Promise.reject(new UserNotFoundError())
      )
    };

    messagesServiceMock = {
      getAll: jest.fn(() => Promise.resolve(MESSAGES))
    };
  });

  it("groups messages by chat id", async () => {
    const chatsService = new ChatsService(
      usersServiceMock,
      messagesServiceMock
    );

    const chats = await chatsService.getAll();

    expect(chats.length).toBe(2);
    expect(chats[0].chat_uuid).toBe(MESSAGES[0].chat_uuid);
    expect(chats[1].chat_uuid).toBe(MESSAGES[1].chat_uuid);
  });

  it("counts messages in each chat", async () => {
    const chatsService = new ChatsService(
      usersServiceMock,
      messagesServiceMock
    );

    const chats = await chatsService.getAll();

    expect(chats[0].messages_count).toBe(3);
    expect(chats[1].messages_count).toBe(1);
  });

  it("returns existing users' full name", async () => {
    const chatsService = new ChatsService(
      usersServiceMock,
      messagesServiceMock
    );

    const chats = await chatsService.getAll();

    expect(chats[1].users).toEqual(["Will Smith"]);
  });

  it("returns non-exitsing users as Anonymous", async () => {
    const chatsService = new ChatsService(
      usersServiceMock,
      messagesServiceMock
    );

    const chats = await chatsService.getAll();

    expect(chats[0].users).toEqual(["John Doe", "Anonymous"]);
  });
});
