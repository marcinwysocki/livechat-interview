const { UsersService, UserNotFoundError } = require("../services/users");

describe("UsersService", () => {
  it("retrieves an existing user by id", async () => {
    const httpClientMock = {
      get: jest.fn(() =>
        Promise.resolve({
          data: {
            user_uuid: "1234-abc",
            first_name: "John",
            last_name: "Doe"
          }
        })
      )
    };
    const usersService = new UsersService("/users", httpClientMock);

    expect(await usersService.getById("1234-abc")).toEqual({
      user_uuid: "1234-abc",
      first_name: "John",
      last_name: "Doe"
    });
    expect(httpClientMock.get).toHaveBeenCalledWith("/users/1234-abc");
  });

  it("throws an error if user doesn't exist", async () => {
    expect.assertions(1);

    const httpClientMock = {
      get: jest.fn(() => Promise.reject())
    };
    const usersService = new UsersService("/users", httpClientMock);

    try {
      await usersService.getById("1234-abc");
    } catch (error) {
      expect(error).toBeInstanceOf(UserNotFoundError);
    }
  });
});
