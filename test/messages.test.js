const { MessagesService } = require("../services/messages");

describe("MessagesService", () => {
  it("retrieves a list of messages", async () => {
    const httpClientMock = {
      get: jest.fn(() =>
        Promise.resolve({
          data: [
            {
              message_uuid: "123",
              chat_uuid: "asd",
              author_uuid: "123-cbs",
              text: "Hi!"
            },
            {
              message_uuid: "ed557979",
              chat_uuid: "9999",
              author_uuid: "lkj12-13",
              text: "See you later!"
            }
          ]
        })
      )
    };
    const messagesService = new MessagesService("/messages", httpClientMock);

    expect(await messagesService.getAll()).toEqual([
      {
        message_uuid: "123",
        chat_uuid: "asd",
        author_uuid: "123-cbs",
        text: "Hi!"
      },
      {
        message_uuid: "ed557979",
        chat_uuid: "9999",
        author_uuid: "lkj12-13",
        text: "See you later!"
      }
    ]);
    expect(httpClientMock.get).toHaveBeenCalledWith("/messages");
  });
});
