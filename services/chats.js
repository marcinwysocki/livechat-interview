const { groupBy, uniq, zip } = require("lodash");

class ChatsService {
  constructor(usersService, messagesService) {
    this._usersService = usersService;
    this._messagesService = messagesService;

    this._fetchUserName = this._fetchUserName.bind(this);
  }

  async getAll() {
    const messages = await this._messagesService.getAll();
    const groupedByChatId = this._groupMessagesByChatId(messages);
    const withUsersDetails = await this._formatUsersDetails(groupedByChatId);

    return withUsersDetails;
  }

  _groupMessagesByChatId(messages) {
    const messagesByChatId = groupBy(messages, "chat_uuid");
    const chatIds = Object.keys(messagesByChatId);

    return chatIds.map(chatId => ({
      chat_uuid: chatId,
      messages_count: messagesByChatId[chatId].length,
      users: messagesByChatId[chatId].reduce(
        (users, message) => [...users, message.author_uuid],
        []
      )
    }));
  }

  async _formatUsersDetails(chats) {
    const userNames = await Promise.all(
      chats.map(({ users }) => Promise.all(users.map(this._fetchUserName)))
    );

    return zip(chats, userNames).reduce(
      (acc, [chat, users]) => [
        ...acc,
        {
          ...chat,
          users: uniq(users)
        }
      ],
      []
    );
  }

  async _fetchUserName(userId) {
    try {
      const { first_name, last_name } = await this._usersService.getById(
        userId
      );

      return `${first_name} ${last_name}`;
    } catch (error) {
      return "Anonymous";
    }
  }
}

module.exports = {
  ChatsService
};
