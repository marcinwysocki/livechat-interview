const axios = require("axios");

class MessagesService {
  constructor(messagesAPIUrl, httpClient = axios) {
    this._messagesAPIUrl = messagesAPIUrl;
    this._httpClient = httpClient;
  }

  async getAll() {
    const { data: messages } = await this._httpClient.get(
      `${this._messagesAPIUrl}`
    );

    return messages;
  }
}

module.exports = { MessagesService };
