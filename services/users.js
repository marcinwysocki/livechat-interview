const axios = require("axios");

class UserNotFoundError extends Error {}

class UsersService {
  constructor(usersAPIUrl, httpClient = axios) {
    this._usersAPIUrl = usersAPIUrl;
    this._httpClient = httpClient;
  }

  async getById(uuid) {
    try {
      const { data: user } = await this._httpClient.get(
        `${this._usersAPIUrl}/${uuid}`
      );

      return user;
    } catch (err) {
      throw new UserNotFoundError("User doesn't exist.");
    }
  }
}

module.exports = { UsersService, UserNotFoundError };
