# Chats Rest API

## Setup

```bash
npm i
```

This project has been developed using Node.js 10.15.1 and requires _at least_ version 7.10+ (`async/await` support).

## Running in developement mode (with mocked dependencies)

```bash
npm run start:dev
```

## Configuration

Port and third-party API URL can be set using `REST_API_PORT` and `THIRD_PARTY_API_URL` environment variables, respectively.

```bash
REST_API_PORT=8080 THIRD_PARTY_API_URL='http://third-party/api' npm run start
```
