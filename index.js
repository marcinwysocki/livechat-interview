const express = require("express");
const morgan = require("morgan");
const axios = require("axios");
const { ChatsService } = require("./services/chats");
const { UsersService } = require("./services/users");
const { MessagesService } = require("./services/messages");

const app = express();
app.use(express.json());
app.use(morgan("dev"));

const PORT = process.env.REST_API_PORT || 4001;
const THIRD_PARTY_API_URL = process.env.THIRD_PARTY_API_URL;

const chatsService = new ChatsService(
  new UsersService(`${THIRD_PARTY_API_URL}/users`),
  new MessagesService(`${THIRD_PARTY_API_URL}/messages`)
);

app.get("/api/chats", async (_req, res) => {
  res.json(await chatsService.getAll());
});

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));
