const express = require("express");
const { USERS, MESSAGES } = require("./test/mocks");

const app = express();
app.use(express.json());

app.get("/api/messages", (_req, res) => {
  res.json(MESSAGES);
});

app.get("/api/users/:uuid", (req, res) => {
  const user = USERS[req.params.uuid];

  if (!user) {
    res.status(404).end();
  } else {
    res.json(user);
  }
});

const PORT = process.env.THIRD_PARTY_REST_API_PORT || 3001;
app.listen(PORT, () =>
  console.log(`Third party API listening on port ${PORT}`)
);
